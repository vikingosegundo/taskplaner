//
//  TaskPlanerApp.swift
//  TaskPlaner
//
//  Created by vikingosegundo on 21.05.24.
//

import SwiftUI

@main
struct TaskPlanerApp: App {
    
    @State private var state = AppState()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environment(state)
        }
    }
}
