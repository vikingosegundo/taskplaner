//
//  ContentView.swift
//  TaskPlaner
//
//  Created by vikingosegundo on 21.05.24.
//

import SwiftUI

struct ContentView: View {
    @Environment(AppState.self) private var state

    @State private var presentTitleInput = false
    @State private var enteredTitle = ""

    var body: some View {
        @Bindable var state = state
        NavigationStack {
            projectList
                .navigationDestination(for:Project.self) {
                    ProjectDetailView(project:$0)
                }
                .navigationTitle("Projects")
                .toolbar {
                    ToolbarItem(placement:.navigationBarTrailing) {
                        Button("Add new Project") {
                            askTitleForProject()
                        }
                    }
                }
                .alert("New Project",
                       isPresented: $presentTitleInput,
                       actions: { alertActions },
                       message: { Text("Please enter title for new Project.") }
            )
        }
    }
    var projectList: some View {
        List {
            ForEach(state.projects) {
                NavigationLink($0.title, value: $0)
            }
            .onDelete(perform:delete)
        }
    }
    var alertActions:some View {
        VStack {
            TextField("title", text:$enteredTitle)
            Button(role:.none  ) { save()  } label: { Text("Add Project") }
            Button(role:.cancel) { reset() } label: { Text("Cancel"     ) }
        }
    }
}
private extension ContentView {
    var cleanedEnteredTitle: String {
        enteredTitle.trimmingCharacters(in:.whitespacesAndNewlines)
    }
    func askTitleForProject() {
        presentTitleInput = true
    }
    func reset() { 
        enteredTitle = ""
    }
    func save() {
        if cleanedEnteredTitle.count > 0 {
            state.alter(by:.adding(.project(Project(title:cleanedEnteredTitle))))
        }
        reset()
    }
    func delete(at offsets: IndexSet) {
        for i in offsets {
            state.alter(by:.removing(.project(state.projects[i])))
        }
    }
}

#Preview {
    ContentView()
}
