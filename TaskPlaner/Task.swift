//
//  Task.swift
//  TaskPlaner
//
//  Created by vikingosegundo on 21.05.24.
//

import Foundation

enum TaskDate {
    case unknown
    case date(Date)
    case timeSpan(TimeSpan)
}
enum TimeSpan {
    case start(from:Date,to:Date)    
}

struct Task {
    enum Commando {
        case changing(Changing); enum Changing {
            case title      (to:String  )
            case description(to:String  )
            case dueDate    (to:TaskDate)
            case completed  (to:Bool    )
        }
    }
    let id         : UUID
    var title      : String
    var description: String
    var dueDate    : TaskDate
    var completed  : Bool
    
    init(title:String) {
        self.init(id:UUID(),title:title,description:"",dueDate:.unknown,completed:false)
    }
    private init(id:UUID,title:String,description:String,dueDate:TaskDate,completed:Bool) {
        self.id          = id
        self.title       = title
        self.description = description
        self.dueDate     = dueDate
        self.completed   = completed
    }
    func alter(by c:Commando) -> Self {
        switch c {
        case let .changing(.title      (to:t)): Self(id:id, title:t,     description:description, dueDate:dueDate, completed:completed)
        case let .changing(.description(to:d)): Self(id:id, title:title, description:d,           dueDate:dueDate, completed:completed)
        case let .changing(.dueDate    (to:d)): Self(id:id, title:title, description:description, dueDate:d,       completed:completed)
        case let .changing(.completed  (to:c)): Self(id:id, title:title, description:description, dueDate:dueDate, completed:c        )
        }
    }
}


extension Task    : Identifiable, Equatable,Hashable {}
extension TaskDate: Equatable,Hashable {}
extension TimeSpan: Equatable,Hashable {}
