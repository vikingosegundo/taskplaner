//
//  AppState.swift
//  TaskPlaner
//
//  Created by vikingosegundo on 23.05.24.
//

import SwiftUI

@Observable class AppState {
    enum Command {
        case adding(Adding); enum Adding {
            case project(Project)
        }
        case removing(Removing); enum Removing {
            case project(Project)
        }
    }
    var projects:[Project]

    convenience init() {
        self.init(projects:[])
    }
    private init(projects:[Project]) {
        self.projects = projects
    }
    
    func alter(by cmd:Command) {
        switch cmd {
        case let   .adding(.project(p)): projects = projects + [p]
        case let .removing(.project(p)): projects = projects.filter { p != $0 }
        }
    }
}
