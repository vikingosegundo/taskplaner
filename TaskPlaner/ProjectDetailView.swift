//
//  ProjectDetailView.swift
//  TaskPlaner
//
//  Created by vikingosegundo on 23.05.24.
//

import SwiftUI

struct ProjectDetailView: View {
    @Bindable var project:Project
    
    @State private var presentTitleInput = false
    @State private var enteredTitle = ""
    
    var body: some View {
        VStack {
            Text(project.title)
            List(project.tasks) { task in
                Text(task.title)
            }
        }.navigationTitle($project.title)
            .alert("New Task",
                   isPresented: $presentTitleInput,
                   actions: {
                TextField("title", text:$enteredTitle)
                Button               {  save() } label: { Text("Add Task") }
                Button(role:.cancel) { reset() } label: { Text("Cancel"  ) }
            },
                   message: { Text("Please enter title for new Task.") }
            )
            .toolbar {
                ToolbarItem(placement:.navigationBarTrailing) {
                    Button("Add Task") {
                        askTitleForTask()
                    }
                }
            }
    }
}
private extension ProjectDetailView {
    var cleanedEnteredTitle: String {
        enteredTitle.trimmingCharacters(in:.whitespacesAndNewlines)
    }
    func askTitleForTask() {
        presentTitleInput = true
    }
    func reset() {
        enteredTitle = ""
    }
    func save() {
        if cleanedEnteredTitle.count > 0 {
            project.alter(by: .adding(.task(Task(title: cleanedEnteredTitle))))
        }
        reset()
    }
}
