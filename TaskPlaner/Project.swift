//
//  Project.swift
//  TaskPlaner
//
//  Created by vikingosegundo on 22.05.24.
//

import Foundation

class Project {
    enum Command {
        case changing(Changing); enum Changing {
            case title(to:String)
        }
        case adding(Adding); enum Adding {
            case task(Task)
        }
    }
    var id:UUID
    var title:String
    var tasks:[Task]
    
    convenience init(title:String) {
        self.init(id:UUID(),title:title, tasks: [])
    }
    private init(id:UUID, title:String, tasks:[Task]) {
        self.id    = id
        self.title = title
        self.tasks = tasks
    }
    
    func alter(by cmd:Command) {
        switch cmd {
        case let .changing(.title(to:t)): title = t
        case let .adding(.task(t))      : tasks.append(t)
        }
    }
}

extension Project: Identifiable,Equatable, Hashable, Observable {
    static func == (lhs: Project, rhs: Project) -> Bool {
        lhs.id == rhs.id
        && lhs.title == rhs.title
        && lhs.tasks == rhs.tasks
    }
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
