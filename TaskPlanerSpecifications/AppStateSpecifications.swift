//
//  AppStateSpecifications.swift
//  TaskPlanerSpecifications
//
//  Created by vikingosegundo on 23.05.24.
//

import Foundation
import Quick
import Nimble
@testable import TaskPlaner

class AppStateSpecifications:QuickSpec {
    override class func spec() {
        describe("AppState") {
            var appState:AppState!
            var p0:Project!
            var p1:Project!
            var p2:Project!
            
            beforeEach {
                appState = AppState()
                p0 = Project(title:"Party")
                p1 = Project(title:"Vacation")
                p2 = Project(title:"Garden")
            }
            afterEach {
                appState = nil
                p0 = nil
                p1 = nil
                p2 = nil
            }
            context("newly instantiated") {
                it("has no projects") { expect(appState.projects).to(beEmpty()) }
            }
            context("adding Project") {
                beforeEach {
                    appState.alter(by: .adding(.project(p0)))
                }
                it("has a project added") { expect(appState.projects).to(equal([p0])) }
                
                context("adding another") {
                    beforeEach {
                        appState.alter(by: .adding(.project(p1)))
                    }
                    it("has 2 projects"){ expect(appState.projects).to(equal([p0,p1])) }
                    
                    context("adding a third") {
                        beforeEach {
                            appState.alter(by: .adding(.project(p2)))
                        }
                        it("has 3 projects"){ expect(appState.projects).to(equal([p0,p1,p2])) }
                    }
                }
            }
            context("removing project") {
                beforeEach {
                    appState.alter(by: .adding(.project(p0)))
                    appState.alter(by: .adding(.project(p1)))
                    appState.alter(by: .adding(.project(p2)))
                    appState.alter(by: .removing(.project(p1)))
                }
                it("has project removed"){ expect(appState.projects).to(equal([p0,p2])) }
            }
            context("updating project") {
                beforeEach {
                    appState.alter(by: .adding(.project(p0)))
                    appState.alter(by: .adding(.project(p1)))
                    appState.alter(by: .adding(.project(p2)))
                    p0.alter(by: .changing(.title(to: "Party!!!")))
                }
                it("has updated project"){ expect(appState.projects.first?.title).to(equal("Party!!!"))}
            }
        }
    }
}
