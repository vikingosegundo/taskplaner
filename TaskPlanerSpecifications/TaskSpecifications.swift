//
//  TaskSpecifications.swift
//  TaskPlanerSpecifications
//
//  Created by vikingosegundo on 21.05.24.
//

import Foundation
import Quick
import Nimble
@testable import TaskPlaner

final class TaskSpecifications: QuickSpec {
    override class func spec() {
        describe("Task") {
            var t0:Task!
            beforeEach {
                t0 = Task(title:"Get Coffee")
            }
            afterEach {
                t0 = nil
            }
            context("newly instantiated") {
                it("has id"               ) { expect(t0.id         ).toNot(beNil())          }
                it("has given title"      ) { expect(t0.title      ).to(equal("Get Coffee")) }
                it("has empty description") { expect(t0.description).to(beEmpty())           }
                it("has unknown due date" ) { expect(t0.dueDate    ).to(equal(.unknown))     }
                it("is not completed"     ) { expect(t0.completed  ).to(beFalse())           }
            }
            context("changing title") {
                var t1:Task!
                beforeEach {
                    t1 = t0.alter(by:.changing(.title(to:"Get Coffee ASAP")))
                }
                afterEach {
                    t1 = nil
                }
                it("has updated title"        ) { expect(t1.title      ).to(equal("Get Coffee ASAP")) }
                it("has changed title"        ) { expect(t1.title      ).toNot(equal(t0.title))       }
                it("has unchanged id"         ) { expect(t1.id         ).to(equal(t0.id))             }
                it("has unchanged description") { expect(t1.description).to(equal(t0.description))    }
                it("has unchanged due date"   ) { expect(t1.dueDate    ).to(equal(t0.dueDate))        }
                it("has unchanged completion" ) { expect(t1.completed  ).to(equal(t0.completed))      }
            }
            context("changing description") {
                var t1:Task!
                beforeEach {
                    t1 = t0.alter(by:.changing(.description(to:"We need a coffee infusion")))
                }
                afterEach {
                    t1 = nil
                }
                it("has updated description" ) { expect(t1.description).to(equal("We need a coffee infusion")) }
                it("has changed description" ) { expect(t1.description).toNot(equal(t0.description))           }
                it("has unchanged id"        ) { expect(t1.id         ).to(equal(t0.id))                       }
                it("has unchanged title"     ) { expect(t1.title      ).to(equal(t0.title))                    }
                it("has unchanged due date"  ) { expect(t1.dueDate    ).to(equal(t0.dueDate))                  }
                it("has unchanged completion") { expect(t1.completed  ).to(equal(t0.completed))                }
            }
            context("changing due date"){
                context("to date") {
                    var t1:Task!
                    let now = Date.now
                    beforeEach {
                        t1 = t0.alter(by:.changing(.dueDate(to:.date(now))))
                    }
                    afterEach {
                        t1 = nil
                    }
                    it("has updated due date now" ) { expect(t1.dueDate    ).to(equal(.date(now)))     }
                    it("has changed due date"     ) { expect(t1.dueDate    ).toNot(equal(t0.dueDate))  }
                    it("has unchanged id"         ) { expect(t1.id         ).to(equal(t0.id))          }
                    it("has unchanged title"      ) { expect(t1.title      ).to(equal(t0.title))       }
                    it("has unchanged description") { expect(t1.description).to(equal(t0.description)) }
                    it("has unchanged completion" ) { expect(t1.completed  ).to(equal(t0.completed))   }

                    context("changing back to unknow") {
                        var t2:Task!
                        beforeEach {
                            t2 = t1.alter(by:.changing(.dueDate(to:.unknown)))
                        }
                        afterEach {
                            t2 = nil
                        }
                        it("has updated due date now" ) { expect(t2.dueDate    ).to(equal(.unknown))       }
                        it("has changed due date"     ) { expect(t2.dueDate    ).toNot(equal(t1.dueDate))  }
                        it("has unchanged id"         ) { expect(t2.id         ).to(equal(t1.id))          }
                        it("has unchanged title"      ) { expect(t2.title      ).to(equal(t1.title))       }
                        it("has unchanged description") { expect(t2.description).to(equal(t1.description)) }
                        it("has unchanged completion" ) { expect(t2.completed  ).to(equal(t1.completed))   }
                    }
                }
                context("to time span") {
                    var t1:Task!
                    let now = Date.now
                    let in2hours = now.advanced(by:2*60*60)
                    beforeEach {
                        t1 = t0.alter(by:.changing(.dueDate(to:.timeSpan(.start(from:now,to:in2hours)))))
                    }
                    afterEach {
                        t1 = nil
                    }
                    it("has updated due date timespan") { expect(t1.dueDate    ).to(equal(.timeSpan(.start(from:now,to:in2hours)))) }
                    it("has changed due date"         ) { expect(t1.dueDate    ).toNot(equal(t0.dueDate))                           }
                    it("has unchanged id"             ) { expect(t1.id         ).to(equal(t0.id))                                   }
                    it("has unchanged title"          ) { expect(t1.title      ).to(equal(t0.title))                                }
                    it("has unchanged description"    ) { expect(t1.description).to(equal(t0.description))                          }
                    it("has unchanged completion"     ) { expect(t1.completed  ).to(equal(t0.completed))                            }

                    context("changing back to unknow") {
                        var t2:Task!
                        beforeEach {
                            t2 = t1.alter(by:.changing(.dueDate(to:.unknown)))
                        }
                        afterEach {
                            t2 = nil
                        }
                        it("has updated due date now" ) { expect(t2.dueDate    ).to(equal(.unknown))       }
                        it("has changed due date"     ) { expect(t2.dueDate    ).toNot(equal(t1.dueDate))  }
                        it("has unchanged id"         ) { expect(t2.id         ).to(equal(t1.id))          }
                        it("has unchanged title"      ) { expect(t2.title      ).to(equal(t1.title))       }
                        it("has unchanged description") { expect(t2.description).to(equal(t1.description)) }
                        it("has unchanged completion" ) { expect(t1.completed  ).to(equal(t0.completed))   }
                    }
                }
            }
            context("completing") {
                var t1:Task!
                beforeEach {
                    t1 = t0.alter(by:.changing(.completed(to:true)))
                }
                afterEach {
                    t1 = nil
                }
                it("is completed"             ) { expect(t1.completed  ).to(beTrue())               }
                it("has changed completed"    ) { expect(t1.completed  ).toNot(equal(t0.completed)) }
                it("has unchanged id"         ) { expect(t1.id         ).to(equal(t0.id))           }
                it("has unchanged title"      ) { expect(t1.title      ).to(equal(t0.title))        }
                it("has unchanged description") { expect(t1.description).to(equal(t0.description))  }
                it("has unchanged due date"   ) { expect(t1.dueDate    ).to(equal(t0.dueDate))      }
                
                context("uncompleting") {
                    var t2:Task!
                    beforeEach {
                        t2 = t1.alter(by:.changing(.completed(to:false)))
                    }
                    afterEach {
                        t2 = nil
                    }
                    it("is uncompleted"           ) { expect(t2.completed  ).to(beFalse())              }
                    it("has changed completed"    ) { expect(t2.completed  ).toNot(equal(t1.completed)) }
                    it("has unchanged id"         ) { expect(t2.id         ).to(equal(t1.id))           }
                    it("has unchanged title"      ) { expect(t2.title      ).to(equal(t1.title))        }
                    it("has unchanged description") { expect(t2.description).to(equal(t1.description))  }
                    it("has unchanged due date"   ) { expect(t2.dueDate    ).to(equal(t1.dueDate))      }
                }
            }
        }
    }
}
