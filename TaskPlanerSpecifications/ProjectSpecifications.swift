//
//  ProjectSpecifications.swift
//  TaskPlanerSpecifications
//
//  Created by vikingosegundo on 22.05.24.
//

import Foundation
import Quick
import Nimble
@testable import TaskPlaner

class ProjectSpecifications:QuickSpec {
    override class func spec() {
        describe("Project") {
            var project:Project!
            beforeEach {
                project = Project(title:"Party")
            }
            afterEach {
                project = nil
            }
            context("newly instantiated") {
                it("has an id"      ) { expect(project.id   ).toNot(beNil())     }
                it("has given title") { expect(project.title).to(equal("Party")) }
                it("has empty tasks") { expect(project.tasks).to(beEmpty())      }
            }
            context("changing title") {
                beforeEach {
                    project.alter(by: .changing(.title(to: "Party!!!")))
                }
                it("has updated title") { expect(project.title).to(equal("Party!!!")) }
                it("has empty tasks"  ) { expect(project.tasks).to(beEmpty())         }
            }
            context("adding task") {
                let t = Task(title: "Get Beer")
                beforeEach {
                    project.alter(by: .adding(.task(t)))
                }
                it("has added task" ) { expect(project.tasks).to(equal([t]))     }
                it("has given title") { expect(project.title).to(equal("Party")) }
            }
        }
    }
}
